package belajar.java.dasar;

public class TipeDataArray {

    public static void main(String[] args) {

        String [] cobaArray = new String[3];

        cobaArray[0]= "M";
        cobaArray[1]= "R";
        cobaArray[2]= "A";

        System.out.println(cobaArray[0]);
        System.out.println(cobaArray[1]);
        System.out.println(cobaArray[2]);

        String[] namaPanjang = {"Mochamad","Rizki","Adityo"};
        System.out.println(namaPanjang[0]);
        System.out.println(namaPanjang[1]);
        System.out.println(namaPanjang[2]);

        int a = namaPanjang.length;
        System.out.println(a);

        String[][] dataMsh = {
                {"Mochamad Rizki Adityo","CR7"},
                {"2211297","2211277"},
                {"IT","IT"}
        };
        System.out.println(dataMsh[0][0]);
        System.out.println(dataMsh[1][2]);

    }
}
