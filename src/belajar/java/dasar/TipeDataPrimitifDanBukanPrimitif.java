package belajar.java.dasar;

public class TipeDataPrimitifDanBukanPrimitif {

    public static void main(String[] args) {

        // Bukan Primitif = tidak ada default value alias bisa null
        Integer iniInteger = 100;
        Long iniLong = 1000L;
        Byte iniByte = 10;

        // Primitif = ada default value yaitu 0
        int iniInteger2 = 100;
        long iniLong2 = 1000;
        byte iniByte2 = 10;

        // Konversi bukan primitis ke primitif
        int iniInteger3 = iniInteger;
        // Konversi primitis ke bukan primitif
        Integer iniInteger4 = iniInteger2;

        // atau
        int age = 30;
        Integer ageObject = age;
        int ageAgain = ageObject;

        // klo mau konversi dri integer ke short yg bukan primitif ke primitif
        short shortAge = ageObject.shortValue();
        
    }
}
