package belajar.java.dasar;

public class SwitchLambda {
    public static void main(String[] args) {

        var nilai = "A";
        String ucapan;

        // Suppot java +14 saja

//        switch(nilai){
//            case "A" -> ucapan="Lulus";
//            case "B","C" -> ucapan="Lulus Cukup";
//            default -> ucapan="Belum Lulus";
//        }

//        System.out.println(ucapan);

        // dengan cara yield

//        ucapan = switch(nilai){
//            case "A" : yield "Lulus";
//            case "B","C" : yield "Lulus Cukup";
//            default : yield "Belum Lulus";
//        }
//        System.out.println(ucapan);

    }
}
