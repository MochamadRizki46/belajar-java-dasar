package belajar.java.dasar;

// A.K.A Variabel Argumen
public class VariableTanpaBatasNilai {

    public static void main(String[] args) {

        int[]values = {80,80,85,90};
        sayCongrats("Rizki", values);

        sayCongrats2("Adityo", 90, 95, 99, 100, 98);
    }

    //kalau tanpa dengan variable Argumen
    static void sayCongrats(String nama, int[] values){

        var total = 0;

        for (var value: values){
            total += value;
        }

        var finalvalue = total / values.length;

        if (finalvalue >= 75){
            System.out.println("Selamat "+ nama + ", Anda luluuuuus");
        } else {
            System.out.println("Maaf "+ nama + ", anda belum lulus");
        }
    }

    //kalau dengan variable Argumen
    static void sayCongrats2(String nama, int... values){

        var total = 0;

        for (var value: values){
            total += value;
        }

        var finalvalue = total / values.length;

        if (finalvalue >= 75){
            System.out.println("Selamat "+ nama + ", Anda luluuuuus");
        } else {
            System.out.println("Maaf "+ nama + ", anda belum lulus");
        }
    }
}
