package belajar.java.dasar;

import java.util.Scanner;

public class LoopingForEach {

    public static void main(String[] args) {

        Scanner r = new Scanner(System.in);
        int a, i, j;

        System.out.print("Input jml nilai: "); a = r.nextInt();

        String [] nama = new String[a];

        for (i=0; i<=a-1; i++){
            System.out.print("Date ke-"+(i+1)+" = "); nama[i] = r.next();
        }

        System.out.println();
        System.out.println();

        for (j=0; j<=a-1; j++){
            System.out.println("Isi Data ke-"+(j+1)+" adalah = "+nama[j]);
        }

        System.out.println();
        System.out.println();

        for (var coba: nama){
            System.out.println("Pake For Each = "+coba);
        }
    }
}
