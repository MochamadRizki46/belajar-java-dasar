package belajar.java.dasar;

public class TernaryOperator {

    public static void main(String[] args) {

        var nilai = 75;

        // valuenya   = kondisinya ? Nilai klo benar     : Nilai klo salah
        String ucapan = nilai>=75  ? "Selamat anda Lulus":"Silakan coba lagi";

        System.out.println(ucapan);
    }
}
