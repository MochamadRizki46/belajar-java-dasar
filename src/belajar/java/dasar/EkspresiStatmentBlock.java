package belajar.java.dasar;

import java.util.Date;

// Ini adlh BLOCK dgn nama EkspresiStatementBlock, diawali-diakhiri dgn {}
public class EkspresiStatmentBlock {

    public static void main(String[] args) {

        //Ini adlh Ekspresi(yg hasilkan single value)... Ekspresi = kata
        int nilai;
        nilai = 10;

        //ini adlh Statement... Statment = kalimat
        System.out.println(nilai);

        //Assigment statemen;
        nilai=11;
        //increment statement
        nilai++;
        //method invocation statment
        System.out.println("Hello World");
        //object creation statment;
        Date tgl = new Date();
    }
}
