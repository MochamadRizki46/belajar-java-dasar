package belajar.java.dasar;

public class OprasiMTK {

    public static void main(String[] args) {

        //pertambahan, perkalian, pembgian, modulus
        int a=9, b=3;
        int c= a+b;
        int d= b-a;
        int e= a*b;
        int f= a/b;

        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println();
        System.out.println();

        // Augmented opratos
        a+=10;  //sama dgn a= a+10;
        System.out.println(a);

        a-=18;  //sama dgn a= a-18;
        System.out.println(a);

        a*=10;  //sama dgn a= a*10;
        System.out.println(a);
        System.out.println();
        System.out.println();


        // Unary oprator
        int h=0;

        h++;  // h+1
        System.out.println(h);

        h--;
        System.out.println(h);
    }
}
