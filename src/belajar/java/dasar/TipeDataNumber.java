package belajar.java.dasar;

public class TipeDataNumber {

    public static void main(String[] args) {

        byte iniByte = 100;
        short iniShort = 1000;
        int iniInt = 10000;
        long iniLong = 100000;

        float iniFloat = 2.00f;
        double iniDouble = 2.0000;

        //Literals number like hexadecimal and binary
        int decimalInt = 25;
        int hexInt = 0xFFFF;  // depan nya 0x
        int binInt = 0b10101; // depan nya 0b

        long balance = 1_000_000_000;
    }
}
