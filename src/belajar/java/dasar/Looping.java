package belajar.java.dasar;

public class Looping {

    public static void main(String[] args) {

        int a, b=0, c=5, d=0;

        for (a=0; a<5; a++){
            System.out.println("Perulangan for loop ke-"+a);
        }

        System.out.println();
        System.out.println();

        while (b<5){
            System.out.println("Perulangan while ke-"+b);
            b++;
        }

        System.out.println();
        System.out.println();

        //bisa minimal run dulu tanpa cek kondisi, cocok untuk menu back or nampilin menu lain
        do{
            System.out.println("Perulangan do while ke-"+c);
            c++;
        } while (c<5);

        System.out.println();
        System.out.println();

        //BREAK AND CONTINOU
        while (true){
            System.out.println("Perulangan break continou ke-"+d);
            d++;
            if (d>5){
                break;
            }
        }
    }
}
